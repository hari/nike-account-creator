function createGmail(callback) {
    var Nightmare = require('nightmare')({
        show: true,
        jar: true,
        headers: {
            'User-Agent': 'Mozilla/5.0 (Linux; U; Android 4.0.3; ko-kr; LG-L160L Build/IML74K) AppleWebkit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30'
        }
    });
    var utils = require('./utils');
    var len = [10, 11, 12, 13, 14, 15];
    var email = utils.randomString(len[Math.floor(Math.random() * len.length)]);
    var dob = utils.getBirthDate().split('/');
    var pass = utils.generatePassword(email);
    Nightmare
        .goto('https://accounts.google.com/SignUp')
        .wait('#FirstName')
        .type('#FirstName', utils.getFirstName())
        .type('#LastName', utils.getFirstName())
        .type('#GmailAddress', email)
        .type('#Passwd', pass)
        .type('#PasswdAgain', pass)
        .insert('#BirthDay', dob[0])
        .insert('#BirthYear', dob[2])
        // .insert('#RecoveryPhoneCountryHolder select', '+1')
        .type('#RecoveryPhoneNumber', utils.getPlivoNumberForGmail())
        .type('#RecoveryEmailAddress', utils.getAnyEmail())
        .insert('#CountryCode', 'US')
        .evaluate(function (month, gender) {
            document.getElementById('HiddenBirthMonth').value = month;
            document.getElementById('HiddenGender').value = gender;
        }, dob[1], (email.length % 2) ? 'MALE' : 'FEMALE')
        //.click('#submitbutton')
        .then(() => {
            console.log('Done');
            callback(null, { gmail: email+'@gmail.com', password: pass })
        })
        .catch(e => {
            if (e) {
                console.log(e);
                callback(e, null, null)
            }
        });
}

module.exports = createGmail;