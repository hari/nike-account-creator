

var nightMare = require('./nmi');
var utils = require('./utils')
var buyPlivoNumber = require('./buy-plivo');

var url = 'https://voice.google.com/u/0/signup';
var auth = [], number;

function buyGoogleVoice(details, callback) {
    auth[0] = details.gmail;
    auth[1] = details.password;
    if (auth.length > 0) {
        var plivoNumber = utils.getPlivoNumber();
        if (plivoNumber == '') {
            console.log('Plivo number is empty.');
            return callback('Plivo number is empty', null);
        }
        nightMare
            .useragent('Mozilla/5.0 (Windows NT 10.0; WOW64; rv:54.0) Gecko/20100101 Firefox/54.0')
            .goto(url)
            .wait('#Email')
            .type('#Email', auth[0])
            .click('#next')
            .wait('#Passwd')
            .type('#Passwd', auth[1])
            .click('#signIn')
            .wait('.md-primary')
            .click('button.md-primary')
            .wait('.shBpM-zRbvQe')
            .click('.shBpM-zRbvQe:first-child')
            .wait(2000)
            //.click('button.md-button:first-child')
            .evaluate(function () {
                var btns = document.querySelectorAll('button');
                for (var i = 0; i < btns.length; i++) {
                    if (btns[i].innerText.toUpperCase() == 'SELECT') {
                        btns[i].click();
                        break;
                    }
                }
                return true;
            })
            .wait(2000)
            //click('button.md-button')
            .evaluate(function () {
                var btns = document.querySelectorAll('button');
                for (var i = 0; i < btns.length; i++) {
                    if (btns[i].innerText.toUpperCase() == 'NEXT') {
                        btns[i].click();
                        break;
                    }
                }
                return true;
            })
            .wait('#input_1')
            .type('#input_1', plivoNumber)
            .click('[aria-label="Send code"]')
            .wait('#input_2')
            .evaluate(function () {
                return document.querySelectorAll('h1')[0].innerText.replace('You selected', '').trim();
            })
            .then(function (numb) {
                number = numb;
                console.log('Done sending verification code.');
                verify(callback);
            })
            .catch(function (eror) {
                if (eror) {
                    console.log(eror);
                    return callback(eror, null);
                }
            });
    }

}

function verify(callback) {
    var code = '123456'.split('');
    nightMare
        .insert('#input_2', code[0])//utils.pullVerificationCode(utils.Source.GOOGLE))
        .insert('#input_3', code[1])//utils.pullVerificationCode(utils.Source.GOOGLE))
        .insert('#input_4', code[2])//utils.pullVerificationCode(utils.Source.GOOGLE))
        .insert('#input_5', code[3])//utils.pullVerificationCode(utils.Source.GOOGLE))
        .insert('#input_6', code[4])//utils.pullVerificationCode(utils.Source.GOOGLE))
        .insert('#input_7', code[5])//utils.pullVerificationCode(utils.Source.GOOGLE))
        .click('[aria-label="Verify"]')
        .then(function () {
            utils.exportNumber(auth[0], auth[1], number);
            console.log('Done verifying.');
            callback(null, { email: auth[0], password: auth[1], number: number});
        })
        .catch(function (er) {
            if (er) {
                console.log(er);
                callback(er, null);
            }
        })
}

module.exports = buyGoogleVoice;